import pygame
from pygame.locals import *
from constant_variables import *
from random import choice
from blocks import *


class Game:

    def __init__(self, resolution, fps, caption):
        pygame.init()
        self.main_screen = pygame.display.set_mode(resolution)
        pygame.display.set_caption(caption)
        game_icon = pygame.image.load("Assets/tetris_icon.png").convert_alpha()
        pygame.display.set_icon(game_icon)
        self.clock = pygame.time.Clock()
        self.fps = fps
        self.resolution = resolution
        self.score = 0
        self.game_running = False
        self.main_menu_running = True
        self.game_over_running = False
        self.pause = False
        self.game_over = False
        self.game_won = False
        self.highest_point = self.resolution[1]
        self.first_placed_block = False
        self.down_velocity = 1
        self.moving_down_fast = False

        # loading tile images
        self.tile_images = {"TURQUOISE": pygame.image.load("Assets/turquoise_tile.png").convert_alpha(),
                            "RED": pygame.image.load("Assets/red_tile.png").convert_alpha(),
                            "BLUE": pygame.image.load("Assets/blue_tile.png").convert_alpha(),
                            "GREEN": pygame.image.load("Assets/green_tile.png").convert_alpha(),
                            "PURPLE": pygame.image.load("Assets/purple_tile.png").convert_alpha(),
                            "YELLOW": pygame.image.load("Assets/yellow_tile.png").convert_alpha(),
                            "ORANGE": pygame.image.load("Assets/orange_tile.png").convert_alpha()}

        # loading background images
        self.game_background = pygame.image.load("Assets/game_background.png").convert_alpha()

        # game sounds
        self.layer_removal_sound = pygame.mixer.Sound("Assets/layer_removed.wav")
        self.game_over_sound = pygame.mixer.Sound("Assets/game_over.wav")
        self.game_won_sound = pygame.mixer.Sound("Assets/victory.wav")
        pygame.mixer.music.load("Assets/main_song.mp3")
        pygame.mixer.music.play(-1)

        # main menu objects
        self.start_game_button = Rect(0, 0, 200, 50)
        self.quit_game_button = Rect(0, 0, 200, 50)
        self.start_game_button.center = (self.resolution[0] // 2, self.resolution[1] // 2 - 100)
        self.quit_game_button.center = (self.resolution[0] // 2, self.resolution[1] // 2 + 100)

        # game objects
        self.side_bar = Rect(resolution[0] - resolution[0] * 0.375 // 10 * 10, 0,
                             resolution[0] * 0.375 // 10 * 10, resolution[1])
        self.all_blocks = [TurquoiseBlock, BlueBlock, OrangeBlock, YellowBlock, GreenBlock, PurpleBlock, RedBlock]

        self.block = choice(self.all_blocks)(self.side_bar.x // 10 * 5, self.side_bar.x // 10 * 2,
                                             self.side_bar.x // 10)
        self.next_block = choice(self.all_blocks)
        self.next_block_preview = self.next_block(self.side_bar.center[0] - self.side_bar.x // 20,
                                                  self.side_bar.x // 10 * 3, self.side_bar.x // 10)
        self.next_block_preview.update_all_blocks()
        self.get_random_block()

        self.block_collision_points = self.block.get_collision_points()
        self.new_block_available = False
        self.all_placed_blocks = []
        self.check_block = Rect(0, self.resolution[1] - self.side_bar.x // 10,
                                self.side_bar.x // 10, self.side_bar.x // 10)

        # in game buttons
        self.return_mm_button = Rect(0, 0, 150, 50)
        self.pause_button = Rect(0, 0, 150, 50)
        self.return_mm_button.center = self.side_bar.center
        self.pause_button.center = self.side_bar.center + pygame.math.Vector2(0, -100)

        # fonts
        self.score_font = pygame.font.SysFont("jokerman", 32)
        self.main_font = pygame.font.SysFont("bauhaus93", 32)
        self.in_game_menu_font = pygame.font.SysFont("bauhaus93", 20)
        self.stop_screen_font = pygame.font.SysFont("maiandragd", 20)
        self.game_over_font = pygame.font.SysFont("maiandragd", 52)
        self.show_score_font = pygame.font.SysFont("maiandragd", 32)

        self.score_score = self.score_font.render(str(self.score), True, BLACK)
        self.score_text = self.main_font.render("Score:", True, BLACK)
        self.start_button_text = self.main_font.render("Start Game", True, BLACK)
        self.quit_button_text = self.main_font.render("Quit Game", True, BLACK)
        self.return_mm_text = self.in_game_menu_font.render("Main Menu", True, BLACK)
        self.pause_text = self.in_game_menu_font.render("Pause", True, BLACK)
        self.pause_screen_text = self.stop_screen_font.render("Game is paused. Click anywhere to continue.",
                                                              True, BLACK)
        self.game_over_text = self.game_over_font.render("GAME OVER", True, BLACK)
        self.continue_text = self.stop_screen_font.render("Click anywhere to return to main menu.", True, BLACK)
        self.game_won_text = self.game_over_font.render("YOU WON", True, BLACK)

        self.start_button_rect = self.start_button_text.get_rect()
        self.start_button_rect.center = self.start_game_button.center
        self.quit_button_rect = self.quit_button_text.get_rect()
        self.quit_button_rect.center = self.quit_game_button.center
        self.return_mm_rect = self.return_mm_text.get_rect()
        self.return_mm_rect.center = self.return_mm_button.center
        self.pause_rect = self.pause_text.get_rect()
        self.pause_rect.center = self.pause_button.center
        self.pause_screen_rect = self.pause_screen_text.get_rect()
        self.pause_screen_rect.center = pygame.math.Vector2(self.resolution[0] // 2,
                                                            self.resolution[1] // 2) + pygame.math.Vector2(0, 100)
        self.game_over_rect = self.game_over_text.get_rect()
        self.game_over_rect.center = pygame.math.Vector2(self.resolution[0] // 2,
                                                         self.resolution[1] // 2) + pygame.math.Vector2(0, -100)
        self.continue_rect = self.continue_text.get_rect()
        self.continue_rect.center = pygame.math.Vector2(self.resolution[0] // 2,
                                                        self.resolution[1] // 2) + pygame.math.Vector2(0, 100)
        self.game_won_rect = self.game_won_text.get_rect()

    def get_random_block(self):
        self.block = choice(self.all_blocks)(self.side_bar.x // 10 * 5, self.side_bar.x // 10 * 2,
                                             self.side_bar.x // 10)
        self.next_block = choice(self.all_blocks)
        self.next_block_preview = self.next_block(self.side_bar.center[0] - self.side_bar.x // 20,
                                                  self.side_bar.x // 10 * 3, self.side_bar.x // 10)
        self.next_block_preview.update_all_blocks()

    def run_game(self):
        while True:
            self.clock.tick(self.fps)
            self.game_events()
            if self.game_running:
                self.draw_game_objects()
            elif self.main_menu_running:
                self.main_menu()
            elif self.game_over or self.game_won:
                self.game_over_screen()
            if self.pause:
                self.game_pause_screen()

    def game_events(self):
        if self.game_running:
            if self.pause:
                move_left = False
                move_right = False
                move_down = False
            else:
                move_left = True
                move_right = True
                move_down = True

                self.block_collision_points = self.block.get_collision_points()
                furthest_side = self.block.get_furthest_side_positions()

                if furthest_side[1] >= self.side_bar.x:
                    move_right = False
                elif furthest_side[0] <= 0:
                    move_left = False

                for block in self.all_placed_blocks:
                    block_collision_points = block.get_collision_points()
                    for level in self.block_collision_points:
                        lt = self.block_collision_points[level].top
                        lb = self.block_collision_points[level].bottom
                        lr = self.block_collision_points[level].right
                        ll = self.block_collision_points[level].left
                        for collision in block_collision_points:
                            ct = block_collision_points[collision].top
                            cb = block_collision_points[collision].bottom
                            if (ct < lb <= cb) or (ct <= lt < cb):
                                cl = block_collision_points[collision].left
                                cr = block_collision_points[collision].right
                                if lr == cl:
                                    move_right = False
                                if ll == cr:
                                    move_left = False

                if self.block_collision_points[list(self.block_collision_points.keys())[-1]].bottom >= \
                        self.resolution[1]:
                    move_down = False
                for block in self.all_placed_blocks:
                    block_collision_points = block.get_collision_points()
                    for level in self.block_collision_points:
                        bl = self.block_collision_points[level].left
                        br = self.block_collision_points[level].right
                        for collision in block_collision_points:
                            if self.block_collision_points[level].bottom == block_collision_points[collision].top:
                                tl = block_collision_points[collision].left
                                tr = block_collision_points[collision].right
                                if not ((bl >= tr and br > tr) or (bl < tl and br <= tl)):
                                    move_down = False
        else:
            move_left = False
            move_right = False
            move_down = False

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                quit()
            if event.type == MOUSEBUTTONUP:
                if event.button == 1:
                    mouse_click = event.pos
                    if self.main_menu_running:
                        if self.start_game_button.left <= mouse_click[0] <= self.quit_game_button.right:
                            if self.start_game_button.top <= mouse_click[1] <= self.start_game_button.bottom:
                                self.main_menu_running = False
                                self.game_running = True
                            elif self.quit_button_rect.top <= mouse_click[1] <= self.quit_button_rect.bottom:
                                pygame.quit()
                                quit()
                    elif self.game_running:
                        if self.return_mm_button.left <= mouse_click[0] <= self.pause_button.right:
                            if self.return_mm_button.top <= mouse_click[1] <= self.return_mm_button.bottom:
                                self.first_placed_block = False
                                self.game_running = False
                                self.main_menu_running = True
                                self.get_random_block()
                                self.score = 0
                                self.all_placed_blocks = []
                                self.score_score = self.score_font.render(str(self.score), True, BLACK)
                                self.down_velocity = 1
                                self.moving_down_fast = False
                            elif self.pause_button.top <= mouse_click[1] <= self.pause_button.bottom:
                                self.pause = True
                                self.game_running = False
                    elif self.pause:
                        self.pause = False
                        self.game_running = True
                    elif self.game_over or self.game_won:
                        pygame.mixer.music.unpause()
                        self.first_placed_block = False
                        self.game_over = False
                        self.main_menu_running = True
                        self.get_random_block()
                        self.score = 0
                        self.all_placed_blocks = []
                        self.score_score = self.score_font.render(str(self.score), True, BLACK)

            if event.type == KEYDOWN:
                if event.key == K_RIGHT:
                    if move_right:
                        self.block.main_block.move_ip(self.block.size, 0)
                        for outer_block in self.block.other_blocks:
                            outer_block.move_ip(self.block.size, 0)
                elif event.key == K_LEFT:
                    if move_left:
                        self.block.main_block.move_ip(-self.block.size, 0)
                        for outer_block in self.block.other_blocks:
                            outer_block.move_ip(-self.block.size, 0)
                elif event.key == K_UP:
                    self.allow_rotation()
                elif event.key == K_DOWN:
                    if move_down:
                        if self.moving_down_fast:
                            self.block.gravity_ind = 0
                            self.moving_down_fast = False
                            self.down_velocity = 1
                        else:
                            self.block.gravity_ind = 0
                            self.moving_down_fast = True
                            self.down_velocity = 0.1

    def main_menu(self):
        self.main_screen.fill(WHITE)

        pygame.draw.rect(self.main_screen, BLACK, self.start_game_button, 3)
        pygame.draw.rect(self.main_screen, BLACK, self.quit_game_button, 3)
        self.main_screen.blit(self.start_button_text, self.start_button_rect)
        self.main_screen.blit(self.quit_button_text, self.quit_button_rect)

        pygame.display.update()

    def game_over_screen(self):
        self.main_screen.fill(DARKER_GRAY)
        if self.game_over:
            self.main_screen.blit(self.game_over_text, self.game_over_rect)
        elif self.game_won:
            self.main_screen.blit(self.game_won_text, self.game_over_rect)
        self.main_screen.blit(self.continue_text, self.continue_rect)

        show_score = self.show_score_font.render(f"Your score was: {str(self.score)}", True, BLACK)
        show_score_rect = show_score.get_rect()
        show_score_rect.center = pygame.math.Vector2(self.resolution[0] // 2, self.resolution[1] // 2)
        self.main_screen.blit(show_score, show_score_rect)

        pygame.display.update()

    def game_pause_screen(self):
        self.main_screen.fill(DARKER_GRAY)
        pause_bar1 = Rect(0, 0, 10, 80)
        pause_bar2 = Rect(0, 0, 10, 80)
        pause_bar1.center = (self.resolution[0] // 2 - 20, self.resolution[1] // 2 - 50)
        pause_bar2.center = (self.resolution[0] // 2 + 20, self.resolution[1] // 2 - 50)
        self.pause_screen_rect.center = (self.resolution[0] // 2, self.resolution[1] // 2 + 25)

        pygame.draw.rect(self.main_screen, BLACK, pause_bar1)
        pygame.draw.rect(self.main_screen, BLACK, pause_bar2)
        self.main_screen.blit(self.pause_screen_text, self.pause_screen_rect)

        pygame.display.update()

    def draw_game_objects(self):
        self.main_screen.blit(self.game_background, (0, 0))
        pygame.draw.rect(self.main_screen, GRAY, self.side_bar)

        if self.new_block_available:
            if not self.first_placed_block:
                self.first_placed_block = True

            self.all_placed_blocks.append(self.block)
            self.block = self.next_block(self.side_bar.x // 10 * 5, self.side_bar.x // 10 * 2,
                                         self.side_bar.x // 10)
            self.next_block = choice(self.all_blocks)
            self.next_block_preview = self.next_block(self.side_bar.center[0] - self.side_bar.x // 20,
                                                      self.side_bar.x // 10 * 3, self.side_bar.x // 10)
            self.next_block_preview.update_all_blocks()
            self.new_block_available = False
            self.moving_down_fast = False
            self.down_velocity = 1

        if not self.pause:
            self.block.move_block_down(self.down_velocity)

        self.block_collision_points = self.block.get_collision_points()
        for block in self.all_placed_blocks:
            block.draw_all_blocks(self.main_screen, self.tile_images[block.tile])
            block_collision_points = block.get_collision_points()
            for level in self.block_collision_points:
                bl = self.block_collision_points[level].left
                br = self.block_collision_points[level].right
                for collision in block_collision_points:
                    if self.block_collision_points[level].bottom == block_collision_points[collision].top:
                        tl = block_collision_points[collision].left
                        tr = block_collision_points[collision].right
                        if not ((bl >= tr and br > tr) or (bl < tl and br <= tl)):
                            if self.block.gravity_ind == int(self.fps * self.down_velocity):
                                self.new_block_available = True

        if self.block_collision_points[list(self.block_collision_points.keys())[-1]].bottom >= self.resolution[1]:
            if self.block.gravity_ind == int(self.fps * self.down_velocity):
                self.new_block_available = True

        for block in self.all_placed_blocks:
            for square in block.all_blocks:
                if square.top < self.block.size * 3:
                    pygame.mixer.music.pause()
                    self.game_over_sound.play()
                    self.game_over = True
                    self.game_running = False

        if self.first_placed_block:
            if not self.all_placed_blocks:
                pygame.mixer.music.pause()
                self.game_won_sound.play()
                self.game_running = False
                self.game_won = True

        self.main_screen.blit(self.score_text, (self.side_bar.x + 10, self.resolution[1] - 200))
        self.main_screen.blit(self.score_score, (self.side_bar.x + 10, self.resolution[1] - 150))

        pygame.draw.rect(self.main_screen, WHITE, self.return_mm_button)
        pygame.draw.rect(self.main_screen, WHITE, self.pause_button)
        pygame.draw.rect(self.main_screen, BLACK, self.return_mm_button, 3)
        pygame.draw.rect(self.main_screen, BLACK, self.pause_button, 3)
        self.main_screen.blit(self.return_mm_text, self.return_mm_rect)
        self.main_screen.blit(self.pause_text, self.pause_rect)

        self.next_block_preview.draw_all_blocks(self.main_screen, self.tile_images[self.next_block_preview.tile])
        self.block.draw_all_blocks(self.main_screen, self.tile_images[self.block.tile])
        self.check_full_row()

        pygame.display.update()

    def check_full_row(self):
        rows_for_removal = {}
        filled_layer = []
        self.check_block = Rect(0, self.resolution[1] - self.side_bar.x // 10,
                                self.side_bar.x // 10, self.side_bar.x // 10)
        row_level = 1
        actual_level = 1
        points = {1: 40, 2: 100, 3: 300, 4: 1200}
        while self.check_block.bottom != 0:
            stop_loop = False
            for block in self.all_placed_blocks:
                for square in block.all_blocks:
                    if self.check_block.center == square.center:
                        self.check_block.x += self.side_bar.x // 10
                        filled_layer.append(square)
                        stop_loop = True
                        break
                if stop_loop:
                    break
            else:
                self.check_block.x = 0
                self.check_block.y -= self.side_bar.x // 10
                filled_layer = []
                actual_level += 1
                row_level = actual_level

            if len(filled_layer) == 10:
                actual_level += 1
                if row_level not in rows_for_removal:
                    rows_for_removal[row_level] = []
                rows_for_removal[row_level] += filled_layer
                filled_layer = []
                self.check_block.x = 0
                self.check_block.y -= self.side_bar.x // 10

        self.remove_filled_layer(rows_for_removal)
        for level in rows_for_removal:
            self.score += points[len(rows_for_removal[level]) // 10] * level

        self.score_score = self.score_font.render(str(self.score), True, BLACK)

    def remove_filled_layer(self, filled_layers):
        if filled_layers:
            for level in filled_layers:
                filled_layer_block_top = filled_layers[level][0].top
                for square in filled_layers[level]:
                    for placed_block in self.all_placed_blocks:
                        if square in placed_block.all_blocks:
                            placed_block.all_blocks.remove(square)
                        if not placed_block.all_blocks:
                            self.all_placed_blocks.remove(placed_block)
                for block in self.all_placed_blocks:
                    for rectangle in block.all_blocks:
                        if rectangle.top < filled_layer_block_top:
                            rectangle.top += self.block.size * (len(filled_layers[level]) // 10)
            self.layer_removal_sound.play()

    def allow_rotation(self):
        if self.game_running and not self.pause:
            rotate = True
            rotation_ind = self.block.rotation_ind - 1
            if rotation_ind < 1:
                rotation_ind = 4

            rotation_values = self.block.get_rotation_values()
            self.block.rotate_block(rotation_values)
            for block in self.block.all_blocks:
                if block.left < 0 or block.right > self.side_bar.x or block.bottom > self.resolution[1]:
                    self.block.rotation_ind = rotation_ind
                    rotation_values = self.block.get_rotation_values()
                    self.block.rotate_block(rotation_values)
                    rotate = False
                    break

            if rotate:
                for block in self.block.all_blocks:
                    for placed_block in self.all_placed_blocks:
                        for square in placed_block.all_blocks:
                            if square.center == block.center:
                                self.block.rotation_ind = rotation_ind
                                rotation_values = self.block.get_rotation_values()
                                self.block.rotate_block(rotation_values)
                                return


if __name__ == '__main__':
    Game(RESOLUTION, FPS, CAPTION).run_game()
