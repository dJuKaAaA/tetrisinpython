import pygame
from pygame.locals import *
from constant_variables import *


class Block(object):

    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size
        self.main_block = Rect(self.x, self.y, self.size, self.size)
        self.other_blocks = [Rect(x, y, size, size),
                             Rect(x, y, size, size),
                             Rect(x, y, size, size)]
        self.gravity_ind = 0
        self.rotation_ind = 2
        self.all_blocks = [self.main_block] + self.other_blocks

    def move_block_down(self, velocity):
        if self.gravity_ind == int(FPS * velocity):
            for block in self.all_blocks:
                block.move_ip(0, self.size)
            self.gravity_ind = 0
        else:
            self.gravity_ind += 1

        self.update_all_blocks()

    def update_all_blocks(self):
        self.all_blocks = [self.main_block] + self.other_blocks

    def rotate_block(self, rotation_values):
        self.other_blocks = [Rect(self.main_block.x + rotation_values[0], self.main_block.y + rotation_values[1],
                                  self.size, self.size),
                             Rect(self.main_block.x + rotation_values[2], self.main_block.y + rotation_values[3],
                                  self.size, self.size),
                             Rect(self.main_block.x + rotation_values[4], self.main_block.y + rotation_values[5],
                                  self.size, self.size)]

        self.rotation_ind += 1
        if self.rotation_ind > 4:
            self.rotation_ind = 1

        self.all_blocks = [self.main_block] + self.other_blocks

    def draw_all_blocks(self, screen, tile):
        for block in self.all_blocks:
            screen.blit(tile, (block.x, block.y))

    def __get_all_levels(self):  # one level is equal to one row of blocks
        levels_blocks_place_holder = {}
        levels_blocks = {}
        for block in self.all_blocks:
            if block.top not in levels_blocks_place_holder:
                levels_blocks_place_holder[block.top] = []
            for level in levels_blocks_place_holder:
                if level == block.top:
                    levels_blocks_place_holder[level].append(block)

        for level in sorted(levels_blocks_place_holder.items()):
            levels_blocks[level[0]] = level[1]

        return levels_blocks

    def get_collision_points(self):
        all_levels = self.__get_all_levels()
        all_levels_points = {}
        level_ind = 1
        for level in all_levels:
            all_levels_points[level_ind] = Rect(0, 0, 0, 0)
            furthest_left = all_levels[level][0].left
            furthest_right = all_levels[level][0].right
            for block in all_levels[level]:
                if block.left < furthest_left:
                    furthest_left = block.left
                if block.right > furthest_right:
                    furthest_right = block.right
            all_levels_points[level_ind] = Rect(furthest_left, all_levels[level][0].top,
                                                furthest_right - furthest_left,
                                                all_levels[level][0].bottom - all_levels[level][0].top)
            level_ind += 1

        return all_levels_points

    def get_furthest_side_positions(self):
        furthest_left = self.all_blocks[0].left
        furthest_right = self.all_blocks[0].right
        for block in self.all_blocks:
            if furthest_right < block.right:
                furthest_right = block.right
            if furthest_left > block.left:
                furthest_left = block.left

        return furthest_left, furthest_right


class TurquoiseBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x - size, y, size, size),
                             Rect(x - size * 2, y, size, size)]
        self.tile = "TURQUOISE"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, -self.size, 0, -self.size * 2, 0)
        elif self.rotation_ind == 2:
            rotation_values = (0, -self.size, 0, self.size, 0, self.size * 2)
        elif self.rotation_ind == 3:
            rotation_values = (-self.size, 0, self.size, 0, self.size * 2, 0)
        else:
            rotation_values = (0, self.size, 0, -self.size, 0, -self.size * 2)

        return rotation_values


class BlueBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x - size, y, size, size),
                             Rect(x - size, y - size, size, size)]
        self.tile = "BLUE"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, -self.size, 0, -self.size, -self.size)
        elif self.rotation_ind == 2:
            rotation_values = (0, self.size, 0, -self.size, -self.size, self.size)
        elif self.rotation_ind == 3:
            rotation_values = (self.size, 0, -self.size, 0, self.size, self.size)
        else:
            rotation_values = (0, self.size, 0, -self.size, self.size, -self.size)

        return rotation_values


class OrangeBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x - size, y, size, size),
                             Rect(x + size, y - size, size, size)]
        self.tile = "ORANGE"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, -self.size, 0, self.size, -self.size)
        elif self.rotation_ind == 2:
            rotation_values = (0, self.size, 0, -self.size, -self.size, -self.size)
        elif self.rotation_ind == 3:
            rotation_values = (self.size, 0, -self.size, 0, -self.size, self.size)
        else:
            rotation_values = (0, self.size, 0, -self.size, self.size, self.size)

        return rotation_values


class YellowBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x, y - size, size, size),
                             Rect(x + size, y - size, size, size)]
        self.tile = "YELLOW"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, 0, -self.size, self.size, -self.size)
        elif self.rotation_ind == 2:
            rotation_values = (-self.size, 0, 0, -self.size, -self.size, -self.size)
        elif self.rotation_ind == 3:
            rotation_values = (-self.size, 0, 0, self.size, -self.size, self.size)
        else:
            rotation_values = (self.size, 0, 0, self.size, self.size, self.size)

        return rotation_values


class GreenBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x, y - size, size, size),
                             Rect(x + size, y - size, size, size),
                             Rect(x - size, y, size, size)]
        self.tile = "GREEN"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (0, -self.size, self.size, -self.size, -self.size, 0)
        elif self.rotation_ind == 2:
            rotation_values = (-self.size, 0, 0, self.size, -self.size, -self.size)
        elif self.rotation_ind == 3:
            rotation_values = (self.size, 0, 0, self.size, -self.size, self.size)
        else:
            rotation_values = (self.size, 0, 0, -self.size, self.size, self.size)

        return rotation_values


class PurpleBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x - size, y, size, size),
                             Rect(x, y - size, size, size)]
        self.tile = "PURPLE"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, -self.size, 0, 0, -self.size)
        elif self.rotation_ind == 2:
            rotation_values = (0, self.size, 0, -self.size, -self.size, 0)
        elif self.rotation_ind == 3:
            rotation_values = (self.size, 0, -self.size, 0, 0, self.size)
        else:
            rotation_values = (0, self.size, 0, -self.size, self.size, 0)

        return rotation_values


class RedBlock(Block):

    def __init__(self, x, y, size):
        Block.__init__(self, x, y, size)
        super().__init__(x, y, size)
        self.other_blocks = [Rect(x + size, y, size, size),
                             Rect(x, y - size, size, size),
                             Rect(x - size, y - size, size, size)]
        self.tile = "RED"

    def get_rotation_values(self):
        if self.rotation_ind == 1:
            rotation_values = (self.size, 0, 0, -self.size, -self.size, -self.size)
        elif self.rotation_ind == 2:
            rotation_values = (-self.size, 0, 0, -self.size, -self.size, self.size)
        elif self.rotation_ind == 3:
            rotation_values = (-self.size, 0, 0, self.size, self.size, self.size)
        else:
            rotation_values = (self.size, 0, 0, self.size, self.size, -self.size)

        return rotation_values
